#!/usr/bin/env python

import pickle

model_file = 'model1.bin'
dictvector_file = 'dv.bin'

with open(model_file, 'rb') as f_in:
     model = pickle.load(f_in)
with open(dictvector_file, 'rb') as f_in:
     dv = pickle.load(f_in)



client = {"reports": 0, "share": 0.001694, "expenditure": 0.12, "owner": "yes"}


X = dv.transform([client])
predict = model.predict_proba(X)[0,1]

print('The probability that this client will get a credit card is',predict)
