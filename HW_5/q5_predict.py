
import pickle
from flask import Flask, request, jsonify

model_file = 'model2.bin'
dictvector_file = 'dv.bin'
with open(model_file, 'rb') as f_in:
     model = pickle.load(f_in)
with open(dictvector_file, 'rb') as f_in:
     dv = pickle.load(f_in)

app = Flask('churn')
@app.route('/predict', methods = ['POST'])


def predict():
    client = request.get_json()
    ##---better put it in a different function
    X = dv.transform([client])
    y_pred = model.predict_proba(X)[:,1][0]
    #initially numpy format to avoid mistake with json,

    result = {
        'card_probability': float(y_pred)}
    return jsonify(result)

if __name__ == "__main__":
    app.run(debug = True, host = '127.0.0.1', port = 9696)