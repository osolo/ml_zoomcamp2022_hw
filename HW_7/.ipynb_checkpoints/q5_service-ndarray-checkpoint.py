import numpy as np
import bentoml
from bentoml.io import JSON
from bentoml.io import NumpyNdarray
from pydantic import BaseModel

class UserProfile(Basemodel):
    name: str
    age: int
    country: str
    rating: int

model_ref = bentoml.xgboost.get("mlzoomcamp_homework:latest")
#dv = model_ref.custom_objects['dictVectorizer']

model_runner = model_ref.to_runner() # - BEntoML abstraction it can scale the model sepately from the rest of the service

svc = bentoml.Service("credit_risk_classifier", runners=[model_runner]) #one can put multiple models in the service


@svc.api(input=NumpyNdarray, output=JSON())
async def classify(vector):
    #application_data = user_profile.dict()
    #vector = dv.transform(application_data)
    prediction = await model_runner.predict.async_run(vector)
    print(prediction)
    result = prediction[0]

    if result > 0.5:
        return {
            "status": "DECLINED"
        }
    elif result > 0.25:
        return {
            "status": "MAYBE"
        }
    else:
        return {
            "status": "APPROVED"
        }