This is solutions to the homeworks for Machine learning Zoomcamp 2022 by Alexey Gregoriev
 - https://github.com/alexeygrigorev/mlbookcamp-code/tree/master/course-zoomcamp
* chapter 1 - intro, pandas -car dataset
* chapter 2 - linear regression / prediction of the house prices
* chapter 3 - logistic regression - housing dataset
* chapter 4 - metrics - roc auc, precision,recall, f1
* chapter 5 - deplyment of logistic regression model - flask/docker
* chapter 6 - decision trees and random forest
* chapter 7 - BentoML